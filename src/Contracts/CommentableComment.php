<?php

namespace Bmartel\Commenter\Contracts;

use Baum\Node;

interface CommentableComment {

	/**
	 * User which posted the comment.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user();

	/**
	 * Reference to the owning model which houses the comments.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\MorphTo
	 */
	public function commentable();

	/**
	 * @param CommentableUser $user
	 * @param Commentable $commentable
	 * @param $content
	 * @param CommentableComment $parentComment
	 * @return CommentableComment
	 */
	public function add(CommentableUser $user, Commentable $commentable, $content, CommentableComment $parentComment = null);

	/**
	 * @param $parentComment
	 * @return mixed
	 */
	public function makeChildOf($parentComment);

}