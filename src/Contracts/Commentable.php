<?php

namespace Bmartel\Commenter\Contracts;


interface Commentable {

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
	 */
	public function comments();

	/**
	 * @param CommentableUser $user
	 * @param $content
	 * @param CommentableComment $parentComment
	 * @return CommentableComment
	 */
	public function addComment(CommentableUser $user, $content, CommentableComment $parentComment = null);
}