<?php
namespace Bmartel\Commenter;

use Baum\Node;
use Bmartel\Commenter\Contracts\Commentable;
use Bmartel\Commenter\Contracts\CommentableComment;
use Bmartel\Commenter\Contracts\CommentableUser;
use Config;

trait CommentableCommentTrait {


	public static function boot() {

		parent::boot();

		// Increment the reference/parent_reference id accordingly
		static::creating(function ($model) {

			$model->incrementReferenceId();
		});
	}

	/**
	 * User which posted the comment.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() {

		return $this->belongsTo(Config::get('auth.model'));
	}

	/**
	 * Reference to the owning model which houses the comments.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\MorphTo
	 */
	public function commentable() {

		return $this->morphTo();
	}

	/**
	 * @param CommentableUser $user
	 * @param Commentable $commentable
	 * @param $content
	 * @param CommentableComment $parentComment
	 * @return mixed
	 */
	public function add(CommentableUser $user, Commentable $commentable, $content, CommentableComment $parentComment = null) {

		$newComment = static::create([
			'user_id' => $user->getKey(),
			'commentable_id' => $commentable->getKey(),
			'commentable_type' => get_class($commentable),
			'content' => $content
		]);

		// Make the comment a child of another, if another comment node is provided.
		if ($parentComment && $newComment instanceof CommentableComment) {
			$newComment->makeChildOf($parentComment);
		}

		return $newComment;
	}

	/**
	 * @param $parentComment
	 * @return mixed
	 */
	public function makeChildOf($parentComment) {

		$comment =  parent::makeChildOf($parentComment);
		$this->setParentReferenceId();

		return $comment;
	}

	/**
	 * Increments the comments' reference id, which is given to refer to the local context
	 * of the object the comment belongs to.
	 */
	public function incrementReferenceId() {

		$lastKey = static
			::where('commentable_id', $this->commentable_id)
			->where('commentable_type', $this->commentable_type)
			->max('reference_id');

		$lastKey = $lastKey ?: 0;

		$this->reference_id = ((int) $lastKey + 1);
	}

	/**
	 * Sets the parent reference id which is used for showing the parent id
	 * in the local context of the object the comment belongs to.
	 */
	public function setParentReferenceId() {

		if ($this->parent_id) {
			$parentReferenceKey = static::where('id', $this->parent_id)->select('reference_id')->first();

			if ($parentReferenceKey) {
				$this->parent_reference_id = (int) $parentReferenceKey->reference_id;
				$this->save();
			}
		}
	}
}