<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $table){

			$table->increments('id');
			$table->unsignedInteger('commentable_id');
			$table->string('commentable_type');
			$table->string('content', 500);
			$table->enum('status', ['hidden', 'visible', 'moderated'])->default('visible');

			// local reference id, for easier sorting of comments on a page.
			$table->unsignedInteger('reference_id')->nullable();

			// local reference to the parent if this is a reply
			$table->unsignedInteger('parent_reference_id')->nullable();

			// true parent reference
			$table->unsignedInteger('parent_id')->nullable();

			// tree nodes
			$table->unsignedInteger('node_left')->nullable();
			$table->unsignedInteger('node_right')->nullable();
			$table->unsignedInteger('node_depth')->nullable();

			$table->timestamps();

			// Indexes
			$table->unique(['id', 'commentable_id', 'commentable_type', 'reference_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}