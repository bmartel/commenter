<?php

namespace Bmartel\Commenter;

use Baum\Node;
use Bmartel\Commenter\Contracts\CommentableComment;

class Comment extends Node implements CommentableComment {

	use CommentableCommentTrait;

	public $table = 'comments';

	// Ensure the nested set is scoped to retrieving by the owningEntity the comments belong to.
	protected $scoped = ['commentable_id', 'commentable_type'];

	// 'parent_id' column name
	protected $parentColumn = 'parent_id';

	// 'lft' column name
	protected $leftColumn = 'node_left';

	// 'rgt' column name
	protected $rightColumn = 'node_right';

	// 'depth' column name
	protected $depthColumn = 'node_depth';

	// guard attributes from mass-assignment
	protected $fillable = ['content', 'reference_id', 'parent_reference_id', 'parent_id', 'node_left', 'node_right', 'node_depth'];

}