<?php

namespace Bmartel\Commenter;

use Config;
use Bmartel\Commenter\Contracts\CommentableComment;
use Bmartel\Commenter\Contracts\CommentableUser;

trait CommentableTrait {

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
	 */
	public function comments() {

		return $this->morphMany(Config::get('commenter.comment_model'), 'commentable');
	}

	/**
	 * @param CommentableUser $user
	 * @param $content
	 * @param CommentableComment $parentComment
	 * @return CommentableComment
	 */
	public function addComment(CommentableUser $user, $content, CommentableComment $parentComment = null) {

		$newComment = $this->comments()->create([
			'user_id' => $user->getKey(),
			'content' => $content
		]);

		if($parentComment && $newComment instanceof CommentableComment) {
			$newComment->makeChildOf($parentComment);
		}

		return $newComment;
	}

}